// Author:
// Title:

#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

uniform sampler2D u_buffer0;

#ifdef BUFFER_0

void main() {
    gl_FragColor = vec4(abs(sin(u_time)));
}

#else

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    st.x *= u_resolution.x/u_resolution.y;

    vec3 color = vec3(0.);
    float b = texture2D(u_buffer0, vec2(0.0, 0.0), 0.0).r;
    color = vec3(st.x,st.y, b);

    gl_FragColor = vec4(color, 1.0);
}

#endif